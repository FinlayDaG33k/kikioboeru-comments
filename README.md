# Admiral-Comments
Module to add commenting to the Admiral Blog module

## Version Compatibility
Below a table of version compatibility.  
Please note that when a new version is released, support for older versions by the maintainer drop *immediately*.
| Plugin Version | Admiral Version | Blog Version | CakePHP Version |
|----------------|-----------------|--------------|-----------------|
| 1.x            | 1.x-2.x         | 1.x          | 3.x             |