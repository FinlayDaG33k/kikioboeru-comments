<?php
  namespace FinlayDaG33k\Comments\Controller;

  use FinlayDaG33k\Comments\Controller\AppController;
  use FinlayDaG33k\Comments\Form\{
    CommentForm
  };
  use Cake\Event\Event;
  use Cake\I18n\FrozenTime;
  use Cake\ORM\Query;
  use Admiral\Admiral\Permission;

  class ApiController extends AppController {
    public function initialize() {
      parent::initialize();

      $this->loadModel('FinlayDaG33k/Comments.ArticlesComments');
      $this->loadModel('Admiral/Blog.Articles');
      $this->loadModel('FinlayDaG33k/Comments.Comments');

      $this->loadComponent('RequestHandler');
      $this->loadComponent('Paginator');

      $this->set('_jsonOptions', JSON_FORCE_OBJECT);

      $this->Auth->allow(['getByArticle', 'getById']);
    }

    public function beforeFilter(Event $event) {
      $this->RequestHandler->renderAs($this, 'json');
    }

    public function getByArticle($article_id = null, $page = 1) {
      if(!$article_id) {
        $this->response->statusCode(422);
        $this->set([
          'status' => 'failure',
          'message' => 'Article id cannot be left empty!',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $article = $this->Articles->findById($article_id)->first();
      if(!$article) {
        $this->response->statusCode(404);
        $this->set([
          'status' => 'failure',
          'message' => 'Article could not be found!',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }
      
      $comments = $this->ArticlesComments
        ->findByArticleId($article_id)
        ->contain('Comments', function(Query $q) {
          $conditions = [['status' => 1]];

          // Check if the user is logged in
          if($this->Auth->user()) {
            $conditions[]['user_id'] = $this->Auth->user('id');

            // Check if the user may view pending comments
            if(Permission::check('finlaydag33k.comments.pending.view', 1)) {
              $conditions[]['status'] = 0;
            }
  
            // Check if the user may view spam comments
            if(Permission::check('finlaydag33k.comments.spam.view', 1)) {
              $conditions[]['status'] = 2;
            }
          }

          

          return $q
            ->where([
              'OR' => $conditions,
            ])
            ->contain([
              'Users', 
              'Reply' => ['Users']
            ]);
        });

      $pageItems = 5;
      $maxPages = intval(ceil($comments->count() / $pageItems));
      if($page > $maxPages) {
        $this->response->statusCode(404);
        $this->set([
          'status' => 'failure',
          'message' => 'Comments page could not be found',
          'result' => [
            'limit' => $maxPages
          ],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $this->set([
        'status' => 'success',
        'message' => '',
        'result' => $this->Paginator->paginate($comments,['limit' => $pageItems,'page' => $page]),
        '_serialize' => [
          'status',
          'message',
          'result'
        ]
      ]);
    }

    public function getById() {
      if(!$this->request->getParam('id')) {
        $this->response->statusCode(400);
        $this->set([
          'status' => 'failure',
          'message' => 'Comment id cannot be left empty!',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $comment = $this->Comments
        ->findById($this->request->getParam('id'))
        ->contain('Reply', function(Query $q) {
          $conditions = [[$this->Comments->Reply->aliasField('status') => 1]];

          // Check if the user is logged in
          if($this->Auth->user()) {
            $conditions[][$this->Comments->aliasField('user_id')] = $this->Auth->user('id');

            // Check if the user may view pending comments
            if(Permission::check('finlaydag33k.comments.pending.view', 1)) {
              $conditions[][$this->Comments->Reply->aliasField('status')] = 0;
            }
        
            // Check if the user may view spam comments
            if(Permission::check('finlaydag33k.comments.spam.view', 1)) {
              $conditions[][$this->Comments->Reply->aliasField('status')] = 2;
            }
          }

          return $q->where(['OR' => $conditions]);
        })
        ->first();
        
      if(!$comment) {
        $this->response->statusCode(404);
        $this->set([
          'status' => 'failure',
          'message' => 'Comment could not be found',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $this->set([
        'status' => 'success',
        'message' => '',
        'result' => $comment,
        '_serialize' => [
          'status',
          'message',
          'result'
        ]
      ]);
    }

    public function add($article_id = null) {
      if(!$article_id) {
        $this->response->statusCode(400);
        $this->set([
          'status' => 'failure',
          'message' => 'Article id cannot be left empty!',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      if(!$this->Auth->user()) {
        $this->response->statusCode(403);
        $this->set([
          'status' => 'failure',
          'message' => 'You must be logged in to post a comment!',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      if(!Permission::check('finlaydag33k.comments.add', 1)) {
        $this->response->statusCode(403);
        $this->set([
          'status' => 'failure',
          'message' => 'You are not allowed to post a comment',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $article = $this->Articles->findById($article_id)->first();
      if(!$article) {
        $this->response->statusCode(404);
        $this->set([
          'status' => 'failure',
          'message' => 'Article could not be found',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $commentForm = new CommentForm();
      if(!$commentForm->validate($this->request->getData())) {
        $this->response->statusCode(422);
        $this->set([
          'status' => 'failure',
          'message' => 'Something went wrong while trying to post your comment',
          'result' => $commentForm->getErrors(),
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $time = FrozenTime::now();
      $comment = $this->Comments->newEntity([
        'user_id' => $this->Auth->user('id'),
        'body' => $this->request->getData('body'),
        'reply' => null,
        'posted' => $time,
        'modified' => $time,
        'status' => 0,
        'articles' => [
          ['id' => $article_id]
        ]
      ]);

      if($this->request->getData('replyto') != null) {
        $comment->reply = intval($this->request->getData('replyto'));
      }

      if(Permission::check('finlaydag33k.comments.auto-approve', 1)) {
        $comment->status = 1;
      }
      
      if(!$this->Comments->save($comment)) {
        $this->response->statusCode(500);
        $this->set([
          'status' => 'failure',
          'message' => 'Something went wrong while trying to post your comment',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $this->eventManager()->dispatch(new Event('FinlayDaG33k/Comments.Comment.posted'), $this, [
        'comment' => ''
      ]);

      $this->set([
        'status' => 'success',
        'message' => 'Your comment has successfully been posted!',
        'result' => [],
        '_serialize' => [
          'status',
          'message',
          'result'
        ]
      ]);
    }

    public function delete($comment_id = null) {
      if(!$comment_id) {
        $this->response->statusCode(422);
        $this->set([
          'status' => 'failure',
          'message' => 'comment id cannot be left empty!',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $comment = $this->Comments->findById($comment_id)->first();
      if(!$comment) {
        $this->response->statusCode(404);
        $this->set([
          'status' => 'failure',
          'message' => 'Comment was not found!',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      if(!$this->Auth->user()) {
        $this->response->statusCode(403);
        $this->set([
          'status' => 'failure',
          'message' => 'You need to be logged in to delete a comment!',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      if(!Permission::check('finlaydag33k.comments.delete', 1)) {
        $this->response->statusCode(403);
        $this->set([
          'status' => 'failure',
          'message' => 'You do not have the permissions to delete this comment',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      if(!$this->Comments->delete($comment)) {
        $this->response->statusCode(500);
        $this->set([
          'status' => 'failure',
          'message' => 'Could not delete the comment!',
          'result' => [],
          '_serialize' => [
            'status',
            'message',
            'result'
          ]
        ]);
        return;
      }

      $this->set([
        'status' => 'success',
        'message' => 'Comment has been deleted!',
        'result' => [],
        '_serialize' => [
          'status',
          'message',
          'result'
        ]
      ]);
    }
  }