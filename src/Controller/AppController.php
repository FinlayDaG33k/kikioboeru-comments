<?php
  namespace FinlayDaG33k\Comments\Controller;

  use Admiral\Blog\Controller\AppController as BaseController;

  class AppController extends BaseController {
    public function initialize(){
      parent::initialize();

      $this->viewBuilder()->setClassName('Admiral/Admiral.App');
    }
  }