<?php
namespace FinlayDaG33k\Comments\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArticlesComments Model
 *
 * @property \FinlayDaG33k\Comments\Model\Table\ArticlesTable|\Cake\ORM\Association\BelongsTo $Articles
 * @property \FinlayDaG33k\Comments\Model\Table\CommentsTable|\Cake\ORM\Association\BelongsTo $Comments
 *
 * @method \FinlayDaG33k\Comments\Model\Entity\ArticlesComment get($primaryKey, $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\ArticlesComment newEntity($data = null, array $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\ArticlesComment[] newEntities(array $data, array $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\ArticlesComment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\ArticlesComment|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\ArticlesComment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\ArticlesComment[] patchEntities($entities, array $data, array $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\ArticlesComment findOrCreate($search, callable $callback = null, $options = [])
 */
class ArticlesCommentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('articles_comments');
        $this->setDisplayField('article_id');
        $this->setPrimaryKey(['article_id', 'comment_id']);

        $this->belongsTo('Articles', [
            'foreignKey' => 'article_id',
            'joinType' => 'INNER',
            'className' => 'Kikioboeru/Kikioboeru.Articles'
        ]);
        $this->belongsTo('Comments', [
            'foreignKey' => 'comment_id',
            'joinType' => 'INNER',
            'className' => 'FinlayDaG33k/Comments.Comments'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['article_id'], 'Articles'));
        $rules->add($rules->existsIn(['comment_id'], 'Comments'));

        return $rules;
    }
}
