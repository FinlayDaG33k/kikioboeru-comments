<?php
namespace FinlayDaG33k\Comments\Model\Entity;

use Cake\ORM\Entity;

/**
 * Comment Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $body
 * @property \Cake\I18n\FrozenTime $posted
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \FinlayDaG33k\Comments\Model\Entity\User $user
 * @property \FinlayDaG33k\Comments\Model\Entity\Article[] $articles
 */
class Comment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'reply' => true,
        'status' => true,
        'body' => true,
        'posted' => true,
        'modified' => true,
        'user' => true,
        'articles' => true
    ];

    protected $_hidden = [
        'user_id',
    ];

    public function isReply() {
        return $this->reply !== null;
    }
}
