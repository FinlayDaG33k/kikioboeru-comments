<?php
namespace FinlayDaG33k\Comments\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArticlesComment Entity
 *
 * @property int $article_id
 * @property int $comment_id
 *
 * @property \FinlayDaG33k\Comments\Model\Entity\Article $article
 * @property \FinlayDaG33k\Comments\Model\Entity\Comment $comment
 */
class ArticlesComment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'article' => true,
        'comment' => true
    ];

    protected $_hidden = [
        'article_id',
        'comment_id'
    ];
}
