<div class="table-responsive">
  <table role="presentation" class="table table-striped">
    <thead>
      <?= $this->Html->tableHeaders([
        [__d('FinlayDaG33k/Comments','Author') => ['class' => 'col-1']], 
        [__d('FinlayDaG33k/Comments','Comment') => ['class' => 'col-4']], 
        [__d('FinlayDaG33k/Comments','In Response To') => ['class' => 'col-2']], 
        [__d('FinlayDaG33k/Comments','Submitted On') => ['class' => 'col-2']],
        [__d('FinlayDaG33k/Comments','Actions') => ['class' => 'col-2']]
      ]); ?>
    </thead>
    <tbody>
      <?php foreach($comments as $comment): ?>
        <tr id="tr-<?= h($comment->id); ?>">
          <td>
            <?php if($comment->user_id): ?>
              <?= h($comment->user['username']); ?>
            <?php else: ?>
              Anonymous
            <?php endif; ?>
          </td>
          <td><?= h(strip_tags($comment->body)); ?></td>
          <td>
            <?= h($comment['articles'][0]->title); ?>
            <?php if($comment->reply): ?>
              <br />
              <small class="text-muted">
                In reply to 
                <?php if($comment->reply->user): ?>
                  <?= $this->Html->link(
                    $comment->reply['user']['username'],
                    '#'
                  ); ?>
                <?php else: ?>
                  Anonymous
                <?php endif; ?>
              </small>
            <?php endif; ?>
          </td>
          <td>
            <?= h($comment->posted); ?>
            <?php if($comment->posted != $comment->modified): ?>
              <br />
              <small class="text-muted">modified on <?= h($comment->posted); ?> </small>
            <?php endif; ?>
          </td>
          <td>
            <?= $this->Html->link(
              $this->Html->tag('i','',['class' => 'fas fa-eye']),
              ['plugin' => 'FinlayDaG33k/Comments', 'controller' => 'Comments', 'action' => 'view', 'id' => $comment->id],
              ['class' => 'btn btn-primary rounded-0', 'escape' => false]
            ); ?>
            <?= $this->Html->link(
              $this->Html->tag('i','',['class' => 'fas fa-reply']),
              '#',
              [
                'class' => 'btn btn-primary rounded-0',
                'escape' => false,
                'data-action' => 'reply',
                'data-comment-id' => $comment->id,
                'data-author' => $comment->user['username'],
                'data-article-id' => $comment->articles[0]->id
              ]
            ); ?>
            <?= $this->Html->link(
              $this->Html->tag('i','',['class' => 'fas fa-trash-alt']),
              '#',
              [
                'class' => 'btn btn-danger rounded-0',
                'data-action' => 'delete',
                'data-comment-id' => $comment->id,
                'escape' => false
              ]
            ); ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>

<div class="modal" tabindex="-1" role="dialog" id="comment-modal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content rounded-0">
      <div class="modal-header">
        <h5 class="modal-title">Reply to comment</h5>
      </div>
      <div class="modal-body">
        <p>
          <form id="comment-form">
            <div class="form-group text-muted">
              <i class="fas fa-reply" aria-hidden="true"></i>
              <span id="reply-to-name"></span>
             </div>
            <div class="form-group">
              <label>Original message</label>
              <textarea id="comment-original" class="form-control rounded-0" rows="5" style="resize: none;" readonly></textarea>
            </div>
            <div class="form-group">
              <label>Write a reply</label>
              <textarea name="body" class="form-control rounded-0" rows="5" style="resize: none;"></textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary rounded-0">
                <i class="fas fa-reply" aria-hidden="true"></i> Reply
              </button>
              <button type="reset" class="btn btn-danger rounded-0">
                <i class="fas fa-trash-alt" aria-hidden="true"></i> Cancel
              </button>
            </div>
            <input type="hidden" id="form-replyto" value="null">
            <input type="hidden" id="form-article" value="null">
          </form>
        </p>
      </div>
    </div>
  </div>
</div>


<script>
  $(document).on('click', '[data-action=delete]', function() {
    var comment_id = $(this).data("comment-id");
    $.ajax({
      url: '/api/comments/' + comment_id,
      method: 'DELETE',
      headers: {
        'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
      }
    })
    .done(function(res) {
      toastr.success("Comment has been removed!");
      $("#tr-" + comment_id).slideUp(300, function(){
        $(this).remove();
      });
    })
    .fail(function(res) {
      toastr.error("Comment could not be removed!");
    });
  });
</script>

<script>
  $(document).on('click', '[data-action=reply]', function() {
    if($(this).data("author")) {
      $("#reply-to-name").text($(this).data("author"))
    } else {
      $("#reply-to-name").text("Anonymous");
    }

    $("#form-replyto").val($(this).data("comment-id"));
    $("#form-article").val($(this).data("article-id"));
    
    $.ajax({
      url: '/api/comments/getById/' + $(this).data('comment-id'),
      method: 'GET',
      headers: {
        'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
      }
    })
    .done(function(res) {
      console.log(res);
      $("#comment-original").text(res.result.body);
    });

    $("#comment-modal").show();
  });

  $(document).on('click', 'button[type=reset]', function() {
    $("#comment-modal").hide();
  });

  $("#comment-form").on('submit', function(event) {
    event.preventDefault();

    var fd = new FormData();
    fd.append("body", $(this).find('textarea[name=body]').val());
    fd.append("replyto", $("#form-replyto").val());

    $.ajax({
      url: '/api/comments/' + $("#form-article").val(),
      method: 'POST',
      headers: {
        'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
      },
      data: fd,
      contentType: false,
      processData: false,
    }).done(function(res) {
      // Reset the form fields
      $("#comment-form").find('textarea[name=body]').val("");
      $("#form-replyto").removeAttr('value');
      $("#reply-to-name").text("");
      $("#comment-modal").hide();

      // Show a toast
      toastr.success("Your comment has been posted!");
    }).fail(function(res) {
      toastr.error("Something went wrong while trying to post your comment");
    });
  });
</script>