<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddApprovalStatus extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
      $this->table('comments')
      ->addColumn('status', 'integer', ['default' => 0, 'limit' => 1, 'null' => false])
      ->save();
  }
}
