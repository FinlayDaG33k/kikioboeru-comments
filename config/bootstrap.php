<?php
  use Cake\Controller\Controller;
  use Admiral\Admiral\Menu;

  if(PHP_SAPI === 'fpm-fcgi'){
    $controller = new Controller();

    Menu::add('main_menu', [
      'name' => 'comments',
      'label' => __('Comments'),
      'icon' => 'fas fa-comments',
      'url' => [
        'plugin'=>'FinlayDaG33k/Comments',
        'controller' => 'comments',
        'action' => 'index'
      ]
    ]);

    // Add the requires associations
    $controller->loadModel('Admiral/Blog.Articles');
    $controller->Articles->belongsToMany('Comments', [
      'foreignKey' => 'article_id',
      'targetForeignKey' => 'comment_id',
      'joinTable' => 'articles_comments',
      'className' => 'FinlayDaG33k/Comments.Comments'
    ]);
  }